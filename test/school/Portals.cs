﻿using System;
using System.IO;
using Xunit;
using System.Collections.Generic;
using System.Linq;

namespace CodingInterview.Tests.School
{
	public class Portals
	{
		[Fact]
		public void TestCases()
		{
			Assert.Equal(
				10,
				new CodingInterview.School.Portals().Solve(
					new int[] { 7, 4, 7, 3 },
					new (int from, int to)[] {
						(1, 3),
						(2, 4)
					}
				)
			);
		}

		[Theory]
		[MemberData(nameof(Cases), parameters: 20)]
		public void TestFromFile(int fileId)
		{
			var arguments = ParseFile(fileId.ToString("D2"));

			Assert.Equal(arguments.answer, new CodingInterview.School.Portals().Solve(arguments.costs, arguments.connections));
		}

		public static IEnumerable<object[]> Cases(int upTo) => Enumerable.Range(1, upTo).Select(i => new object[] { i });

		(int[] costs, (int from, int to)[] connections, int answer) ParseFile(string name)
		{
			// Open the text file using a stream reader.
			using (var sr = File.OpenText($"resources/portals/{name}.in"))
			{
				// Read the stream to a string, and write the string to the console.
				sr.ReadLine();

				var costs = sr.ReadLine().Split(' ').Select(s => Int32.Parse(s));

				string line;
				var connections = new LinkedList<(int from, int to)>();

				while ((line = sr.ReadLine()) != null)
				{
					var asArray = line.Split(' ');
					connections.AddFirst((int.Parse(asArray[0]), int.Parse(asArray[1])));
				}

				using (var streamAns = File.OpenText($"resources/portals/{name}.ans"))
				{
					// Read the stream to a string, and write the string to the console.
					var ans = int.Parse(streamAns.ReadLine());

					return (costs.ToArray(), connections.ToArray(), ans);
				}
			}

		}
	}
}
