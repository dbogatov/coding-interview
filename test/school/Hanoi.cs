﻿using Xunit;
using CodingInterview.School;

namespace CodingInterview.Tests.School
{
	public class Hanoi
	{
		[Fact]
		public void TestCases()
		{
			Assert.Equal(
				"1->2\n1->3\n2->3\n1->2\n3->1\n3->2\n1->2\n",
				CodingInterview.School.Hanoi.Solve(3)
			);
		}
	}
}
