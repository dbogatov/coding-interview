﻿using Xunit;
using CodingInterview.School;

namespace CodingInterview.Tests.School
{
	public class Children
	{
		[Fact]
		public void TestCases()
		{
			Assert.Equal(4, CodingInterview.School.Children.Solve(10, 3));
			Assert.Equal(5, CodingInterview.School.Children.Solve(10, 2));
			Assert.Equal(10, CodingInterview.School.Children.Solve(10, 1));
			Assert.Equal(1, CodingInterview.School.Children.Solve(1, 10));
			Assert.Equal(1, CodingInterview.School.Children.Solve(2, 10));
		}
	}
}
