﻿using System;
using System.Linq;
using Xunit;
using CodingInterview.Sort;

namespace CodingInterview.Tests.Sort
{
	public class QuickSort
	{
		[Fact]
		public void TestCases()
		{
			void CheckSorted(int[] input)
			{
				new CodingInterview.Sort.QuickSort().Sort(input);
				if (input.Length >= 2)
				{
					for (int i = 1; i < input.Length; i++)
					{
						Assert.True(input[i] >= input[i - 1]);
					}
				}
			}

			CheckSorted(new int[] { -1, 1, 2, 5, 8, 9, 45 });
			CheckSorted(new int[] { 2, 5, 6, 8, 9 });
			CheckSorted(new int[] { 1 });
			CheckSorted(new int[] { });
			CheckSorted(new int[] { 5, 5, 6 });

			var rand = new Random(1305);
			for (int i = 0; i < 20; i++)
			{
				var input = Enumerable.Repeat(0, 25).Select(i => rand.Next(-100, 100)).ToArray();
				CheckSorted(input);
			}
		}
	}
}
