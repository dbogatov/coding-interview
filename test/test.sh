#!/usr/bin/env bash

# A helper script for passing filters to dotnet test or spawning SSE servers

set -e
shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

usage() { echo "Usage: $0 [-n <string>]" 1>&2; exit 1; }


echo "Changing environment to testing..."
export ASPNETCORE_ENVIRONMENT="Testing"

while getopts "n:p:dae" o; do
	case "${o}" in
		n)
			NAME="--filter ${OPTARG}"
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))

dotnet build

echo "Running dotnet tests..."

dotnet test --no-build --verbosity n $NAME

echo "Testing completed!"
