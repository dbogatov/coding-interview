using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class CountTheIslands
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(
				5,
				new CodingInterview.HackerRank.CountTheIslands().Solve(
					new int[,] {
						{1, 1, 0, 0, 0},
						{0, 1, 0, 0, 1},
						{1, 0, 0, 1, 1},
						{0, 0, 0, 0, 0},
						{1, 0, 1, 0, 1}
					}
				)
			);
		}
	}
}
