using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class FindStrings
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(
				new string[] { "a", "bc", "INVALID" },
				new CodingInterview.HackerRank.FindStrings().Solve(new string[] { "abc", "cde" }, new int[] { 1, 5, 20 }));
			Assert.Equal(
				new string[] { "aab", "c", "INVALID" },
				new CodingInterview.HackerRank.FindStrings().Solve(new string[] { "aab", "aac" }, new int[] { 3, 8, 23 }));
			// This one is my own
			Assert.Equal(
				new string[] { "a", "abc", "cde", "dez", "fh", "z", "INVALID" },
				new CodingInterview.HackerRank.FindStrings().Solve(new string[] { "abc", "cdez", "cfe", "cfh" }, new int[] { 1, 3, 8, 15, 20, 22, 25 }));
		}
	}
}
