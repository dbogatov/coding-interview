﻿using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class LIS
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(3, new CodingInterview.HackerRank.LIS().Solve(new int[] { 2, 7, 4, 3, 8 }));
			Assert.Equal(4, new CodingInterview.HackerRank.LIS().Solve(new int[] { 2, 4, 3, 7, 4, 5 }));
			Assert.Equal(6, new CodingInterview.HackerRank.LIS().Solve(new int[] { 15, 27, 14, 38, 26, 55, 46, 65, 85 }));
		}
	}
}
