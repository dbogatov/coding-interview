﻿using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class ConstructArray
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(3, new CodingInterview.HackerRank.ConstructArray().Solve(4, 3, 2));
		}
	}
}
