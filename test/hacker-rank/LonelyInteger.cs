﻿using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class LonelyInteger
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(4, new CodingInterview.HackerRank.LonelyInteger().Solve(new int[] { 1, 2, 3, 4, 3, 2, 1 }));
		}
	}
}
