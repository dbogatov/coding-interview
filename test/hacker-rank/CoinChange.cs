﻿using System.Collections.Generic;
using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class CoinChange
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(3, new CodingInterview.HackerRank.CoinChange().Solve(3, new List<long> { 8, 3, 1, 2 }));
			Assert.Equal(4, new CodingInterview.HackerRank.CoinChange().Solve(4, new List<long> { 1, 2, 3 }));
			Assert.Equal(5, new CodingInterview.HackerRank.CoinChange().Solve(10, new List<long> { 2, 5, 3, 6 }));
		}
	}
}
