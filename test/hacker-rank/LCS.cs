﻿using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class LCS
	{
		[Fact]
		public void TestCases()
		{
			void checkSolution(int[] a, int[] b, int lcsLength)
			{
				bool isSubSequence(int[] subseq, int[] seq)
				{
					int j = 0;

					for (int i = 0; i < seq.Length && j < subseq.Length; i++)
					{
						if (subseq[j] == seq[i])
						{
							j++;
						}
					}

					return (j == subseq.Length);
				}

				var lcs = new CodingInterview.HackerRank.LCS().Solve(a, b);

				Assert.Equal(lcsLength, lcs.Length);
				Assert.True(isSubSequence(lcs, a));
				Assert.True(isSubSequence(lcs, b));
			}

			// From Hacker Rank
			checkSolution(
				new int[] { 1, 2, 3, 4, 1 },
				new int[] { 3, 4, 1, 2, 1, 3 },
				3
			);
			checkSolution(
				new int[] { 3, 9, 8, 3, 9, 7, 9, 7, 0 },
				new int[] { 3, 3, 9, 9, 9, 1, 7, 2, 0, 6 },
				6
			);
		}
	}
}
