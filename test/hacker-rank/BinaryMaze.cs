using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class BinaryMaze
	{
		[Fact]
		public void TestCaseOne()
		{
			// From Hacker Rank
			VerifySolution(
				new char[,] {
					{'Y', '0', '0', '0', '0'},
					{'1', '1', '1', '0', '0'},
					{'0', '0', '1', '0', '0'},
					{'0', '0', '1', 'X', '0'}
				}, 6, (i: 0, j: 0)
			);
		}

		[Fact]
		public void TestCaseTwo()
		{
			// From Hacker Rank
			VerifySolution(
				new char[,] {
					{'1', '1', '0', 'X', '1', '0'},
					{'0', '1', 'Y', '0', '1', '1'},
					{'0', '1', '0', '0', '0', '1'},
					{'0', '1', '0', '0', '0', '1'},
					{'0', '1', '1', '1', '1', '1'},
					{'0', '1', '0', '0', '0', '0'},
				}, 14, (i: 1, j: 2)
			);
		}

		private void VerifySolution(char[,] map, int shortest, (int i, int j) start)
		{
			var answer = new CodingInterview.HackerRank.BinaryMaze().Solve(start, map);

			Assert.Equal(shortest, answer.Count);
			var location = start;
			foreach (var step in answer)
			{
				if (
					location.i < 0 || location.i == map.GetLength(0) ||
					location.j < 0 || location.j == map.GetLength(1) ||
					map[location.i, location.j] == '0'
				)
				{
					Assert.False(true, "illegal tile");
				}

				switch (step)
				{
					case 'u':
						location = (i: location.i - 1, j: location.j);
						break;
					case 'd':
						location = (i: location.i + 1, j: location.j);
						break;
					case 'l':
						location = (i: location.i, j: location.j - 1);
						break;
					case 'r':
						location = (i: location.i, j: location.j + 1);
						break;
					default:
						Assert.False(true, "unknown step");
						break;
				}
			}
			Assert.Equal('X', map[location.i, location.j]);
		}
	}
}
