using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class PlayingWithNumbers
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(
				new int[] { 5, 7, 6 },
				new CodingInterview.HackerRank.PlayingWithNumbers().Solve(
					new int[] { -1, 2, -3 },
					new int[] { 1, -2, 3 }
				)
			);
			Assert.Equal(
				new int[] { 20, 25, 21, 28, 20 },
				new CodingInterview.HackerRank.PlayingWithNumbers().Solve(
					new int[] { -3, 2, -5, 4, 8 },
					new int[] { -2, -3, 4, 5, -6 }
				)
			);
		}
	}
}
