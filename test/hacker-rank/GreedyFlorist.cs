using Xunit;

namespace CodingInterview.Tests.HackerRank
{
	public class GreedyFlorist
	{
		[Fact]
		public void TestCases()
		{
			// From Hacker Rank
			Assert.Equal(13, new CodingInterview.HackerRank.GreedyFlorist().Solve(3, new int[] { 2, 5, 6 }));
			Assert.Equal(15, new CodingInterview.HackerRank.GreedyFlorist().Solve(2, new int[] { 2, 5, 6 }));
			Assert.Equal(29, new CodingInterview.HackerRank.GreedyFlorist().Solve(3, new int[] { 1, 3, 5, 7, 9 }));
		}
	}
}
