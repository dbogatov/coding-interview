﻿namespace CodingInterview.Sort
{
	public class QuickSort
	{
		public void Sort(int[] input)
		{
			if (input.Length >= 2)
			{
				Quick(0, input.Length - 1, input);
			}
		}

		private void Swap(int[] input, int a, int b)
		{
			var tmp = input[a];
			input[a] = input[b];
			input[b] = tmp;
		}

		private void Quick(int left, int right, int[] input)
		{
			if (left >= right)
			{
				return;
			}

			var pivotIndex = (right + left) / 2;
			var pivotValue = input[pivotIndex];

			var smaller = left;
			var bigger = right;

			while (smaller < bigger)
			{
				if (input[smaller] <= pivotValue)
				{
					smaller++;
				}
				else if (input[bigger] >= pivotValue)
				{
					bigger--;
				}
				else
				{
					Swap(input, smaller, bigger);
				}
			}
			if (
				pivotIndex > smaller && input[smaller] > pivotValue ||
				pivotIndex < smaller && input[smaller] < pivotValue
			)
			{
				Swap(input, smaller, pivotIndex);
				pivotIndex = smaller;
			}
			else if (
				pivotIndex < smaller && input[smaller] > pivotValue
			)
			{
				Swap(input, smaller - 1, pivotIndex);
				pivotIndex = smaller - 1;
			}

			Quick(left, pivotIndex - 1, input);
			Quick(pivotIndex + 1, right, input);
		}
	}
}
