﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingInterview.School
{
	/// <summary>
	/// cSpell:disable
	/// Деякі планети галактики, куди нещодавно переїхав Петрик П’яточкін, сполучено порталами.
	/// Якщо планети А і Б сполучено, це означає, що на кожній із планет стоїть спеціальний пристрій — портал — для телепортації між ними.
	/// Істота, що входить у портал на планеті А, миттєво опиняється на планеті Б і навпаки.
	///
	/// Один і той самий портал не можна використовувати для телепортації на різні планети.
	/// Якщо між парою планет ще немає сполучення, їх можна сполучити, але лише побудувавши на кожній із них по новому порталу.
	/// Будівництво порталів вимагає чималих витрат і може коштувати по-різному на різних планетах.
	///
	/// Будемо казати, що між двома планетами існує шлях телепортації, якщо з однієї планети можна потрапити на іншу, телепортувавшись один або кілька разів (використовуючи проміжні планети).
	/// На жаль, поки що не між кожними двома планетами існує шлях телепортації.
	///
	/// Маючи схему наявного сполучення планет і знаючи вартість будівництва порталу на кожній планеті, визначте, яку найменшу суму грошей потрібно витратити, щоб забезпечити існування шляху телепортації між кожною парою планет галактики.
	/// cSpell:enable
	///
	/// http://www.kievoi.ippo.kubg.edu.ua/kievoi/3/2013.html
	/// </summary>
	public class Portals
	{
		class Planet
		{
			public int Cost { get; set; }
			public List<Planet> Neighbors { get; set; } = new List<Planet>();
			public bool Visited { get; set; } = false;
		}

		/// <summary>
		/// Returns the minmum amount of money needed to get all plannets connected.
		/// </summary>
		/// <param name="costs">the costs of building a portal on each planet (index is a planet)</param>
		/// <param name="connections">pairs of indices representing planets which already have portals between them (here indexaton starts from 1)</param>
		/// <returns>the minmum amount of money needed to get all plannets connected</returns>
		public int Solve(int[] costs, (int from, int to)[] connections)
		{
			// create planets given costs
			var planets = new Planet[costs.Length];
			for (var planet = 0; planet < costs.Length; planet++)
			{
				planets[planet] = new Planet { Cost = costs[planet] };
			}
			// setup connections
			foreach (var connection in connections)
			{
				planets[connection.from - 1].Neighbors.Add(planets[connection.to - 1]);
				planets[connection.to - 1].Neighbors.Add(planets[connection.from - 1]);
			}

			// go over all planets in a BFS manner
			var minCosts = new List<int>();
			for (var planet = 0; planet < costs.Length; planet++)
			{
				// skip visited planets
				if (planets[planet].Visited)
				{
					continue;
				}

				// initialize BFS queue with the initial (unvisited) planet
				var minCost = Int32.MaxValue;
				var bfsQueue = new Queue<Planet>();
				bfsQueue.Enqueue(planets[planet]);

				// until entire cluster is traversed
				while (bfsQueue.Count > 0)
				{
					var current = bfsQueue.Dequeue();
					// skip visited
					if (!current.Visited)
					{
						// update min cost in the cluster
						minCost = Math.Min(current.Cost, minCost);
						// don't go here again
						current.Visited = true;
						// add not visited neighbors to the queue
						foreach (var neighbor in current.Neighbors)
						{
							if (!neighbor.Visited)
							{
								bfsQueue.Enqueue(neighbor);
							}
						}
					}
				}

				// remember min cost of the cluster
				minCosts.Add(minCost);
			}

			// find the smallest of all min costs
			var minOfMin = minCosts.Min();

			// if T is the number of clusters, the final cost is building T - 1 portals in the cheapest cluster on its cheapest planet,
			// then building a portal on a cheapest planet on each of the other clusters
			return minOfMin * (minCosts.Count - 1) + (minCosts.Sum() - minOfMin);
		}
	}
}
