using System.Collections.Generic;
using System.Linq;

namespace CodingInterview.HackerRank
{
	/// <summary>
	/// You are working at the cash counter at a fun-fair, and you have different types of coins available to you in infinite quantities.
	/// The value of each coin is already given.
	/// Can you determine the number of ways of making change for a particular number of units using the given types of coins?
	///
	/// For example, if you have 4 types of coins, and the value of each type is given as 8, 3, 1, 2 respectively, you can make change for 3 units in three ways: {1, 1, 1}, {1, 2}, and {3}.
	///
	/// https://www.hackerrank.com/challenges/coin-change/problem
	/// </summary>
	public class CoinChange
	{
		// memoization cache;
		// the key is (change, number of coins), the value is the number of ways to make change;
		// because the list is always in the same order, the number of coins totally defines which coins there are;
		private Dictionary<(int, int), long> cache = new Dictionary<(int, int), long>();

		/// <summary>
		/// Solve the Coin Change Problem
		/// </summary>
		/// <param name="n">the amount to make change for</param>
		/// <param name="c">available denominations (guaranteed to be unique)</param>
		/// <returns>the number of ways to make change</returns>
		public long Solve(int n, List<long> c)
		{
			// check memoization first
			if (cache.ContainsKey((n, c.Count)))
			{
				return cache[(n, c.Count)];
			}
			else
			{
				long result;

				// n == 0 means that the caller could have used the subtracted coin alone to fullfil the change, thus 1
				if (n == 0)
				{
					result = 1;
				}
				// below 0 change or empty list of denominations means going out of bound of the problem (below the leaf level of the tree)
				else if (n < 0 || c.Count == 0)
				{
					result = 0;
				}
				// else we are somwhere in the middle (or top) of the tree
				else
				{
					// most important, the number of ways one can make change for n given c.Count distinct denominations is like this;
					// the number of ways to make change for n WITHOUT the last coin (thus c without last of c), AND
					// the number of ways to make change for n WITH the last coin (thus, n - last of c)
					result = Solve(n, c.Take(c.Count - 1).ToList()) + Solve(n - (int)c.Last(), c);
				}
				// there is a ton of dupplicate invocations when run recursively, so we use memoization
				cache.Add((n, c.Count), result);
				return result;
			}
		}
	}
}
