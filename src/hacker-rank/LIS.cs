using System;
using System.Collections.Generic;

namespace CodingInterview.HackerRank
{
	/// <summary>
	/// The task is to find the length of the longest subsequence in a given array of integers such that all elements of the subsequence are sorted in strictly ascending order.
	/// This is called the Longest Increasing Subsequence (LIS) problem.
	///
	/// For example, the length of the LIS for [15, 27, 14, 38, 26, 55, 46, 65, 85] is 6 since the longest increasing subsequence is [15, 27 38, 55, 65, 85].
	///
	/// https://www.hackerrank.com/challenges/longest-increasing-subsequent/problem
	/// </summary>
	/// <remarks>
	/// This solution does not pass all tests on HackerRank.
	/// Best solution is O(n log(n)), this one is O(n^2).
	/// </remarks>
	public class LIS
	{
		class CustomComparator : IComparer<(int number, int index)>
		{
			public int Compare((int number, int index) x, (int number, int index) y)
			{
				var numberCompare = x.number.CompareTo(y.number);
				if (numberCompare != 0)
				{
					return numberCompare;
				}
				return x.index.CompareTo(y.index);
			}
		}

		class BST<TKey, TValue>
		{
			private IComparer<TKey> _comparer;
			private Node _root;

			public BST(IComparer<TKey> comparer)
			{
				_comparer = comparer;
			}

			class Node
			{
				public TKey Key;
				public TValue Value;
				public Node Left;
				public Node Right;
			}

			public TValue GetMaxWhichIsSmallerThan(TKey query)
			{
				if (_root == null)
				{
					return default(TValue);
				}

				var @this = _root;
				var max = default(TValue);
				while (true)
				{
					if (_comparer.Compare(query, @this.Key) == 0)
					{
						return @this.Value;
					}
					if (_comparer.Compare(query, @this.Key) < 0)
					{
						if (@this.Left == null)
						{
							return max;
						}
						@this = @this.Left;
						continue;
					}
					if (_comparer.Compare(query, @this.Key) > 0)
					{
						if (@this.Right == null)
						{
							return @this.Value;
						}
						max = @this.Value;
						@this = @this.Right;
						continue;
					}
				}
			}

			public void Add(TKey key, TValue value)
			{
				if (_root == null)
				{
					_root = new Node { Key = key, Value = value };
					return;
				}

				var @this = _root;
				while (true)
				{
					if (_comparer.Compare(key, @this.Key) == 0)
					{
						@this.Value = value;
						return;
					}
					if (_comparer.Compare(key, @this.Key) < 0)
					{
						if (@this.Left == null)
						{
							@this.Left = new Node { Key = key, Value = value };
							return;
						}
						@this = @this.Left;
						continue;
					}
					if (_comparer.Compare(key, @this.Key) > 0)
					{
						if (@this.Right == null)
						{
							@this.Right = new Node { Key = key, Value = value };
							return;
						}
						@this = @this.Right;
						continue;
					}
				}
			}
		}

		/// <summary>
		/// Returns an integer that denotes the array's LIS.
		/// </summary>
		/// <param name="arr">an unordered array of integers</param>
		/// <returns>an integer that denotes the array's LIS</returns>
		public int Solve(int[] arr)
		{
			var DP = new SortedDictionary<(int number, int index), int>(new CustomComparator());
			int max = 0;
			int index = 0;

			foreach (var current in arr)
			{
				int bestSoFar = 0;
				foreach (var dp in DP)
				{
					if (dp.Key.number > current)
					{
						break;
					}
					if (dp.Value > bestSoFar)
					{
						bestSoFar = dp.Value;
					}
				}
				bestSoFar++;
				max = Math.Max(bestSoFar, max);

				DP.Add((current, index), bestSoFar);

				index++;
			}

			return max;
		}
	}
}
