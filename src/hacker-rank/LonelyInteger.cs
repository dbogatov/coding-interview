namespace CodingInterview.HackerRank
{
	/// <summary>
	/// You will be given an array of integers.
	/// All of the integers except one occur twice.
	/// That one is unique in the array.
	///
	/// Given an array of integers, find and print the unique element.
	///
	/// For example, a = [1, 2, 3, 4, 3, 2, 1], the unique element is 4.
	/// </summary>
	public class LonelyInteger
	{
		/// <summary>
		/// Finds the unique element
		/// </summary>
		/// <param name="a">An array of integers where each integer occurs twice except one, which occurs once</param>
		/// <returns>The integer which occurs once</returns>
		public long Solve(int[] a)
		{
			for (var i = 1; i < a.Length; i++)
			{
				a[0] = a[0] ^ a[i];
			}
			return a[0];
		}
	}
}
