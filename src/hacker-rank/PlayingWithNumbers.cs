using System;
using System.Collections.Generic;

namespace CodingInterview.HackerRank
{
	/// <summary>
	/// Given an array of integers, you must answer a number of queries.
	/// Each query consists of a single integer, x, and is performed as follows:
	///
	/// 1. Add x to each element of the array, permanently modifying it for any future queries.
	/// 2. Find the absolute value of each element in the array and print the sum of the absolute values on a new line.
	///
	/// https://www.hackerrank.com/challenges/playing-with-numbers/problem
	/// </summary>
	/// <remarks>
	/// This solution does not pass all tests on HackerRank.
	/// Better solution will likely include an index over array that can immediately find the endpoints of flipped range.
	/// </remarks>
	public class PlayingWithNumbers
	{
		/// <summary>
		/// Returns an array of integers that represent the responses to each query
		/// </summary>
		/// <param name="arr">an array of integers</param>
		/// <param name="queries">an array of integers</param>
		/// <returns>an array of integers that represent the responses to each query</returns>
		public int[] Solve(int[] arr, int[] queries)
		{
			// sort first
			Array.Sort(arr);

			// original sum
			var sum = 0;
			// the index of the first entry >= 0, or the last element
			var index = 0;
			foreach (var e in arr)
			{
				if (e < 0)
				{
					index++;
				}
				sum += Math.Abs(e);
			}
			// put index back in if needed
			if (index == arr.Length)
			{
				index--;
			}

			var result = new List<int>();

			// all queries are summed up, this is a running sum
			var added = 0;

			foreach (var query in queries)
			{
				// each query is answered the same given the running sum
				// q is the query as if it is the first one
				var q = added + query;

				// simple case
				if (q == 0)
				{
					result.Add(sum);
					continue;
				}

				// these are the changes caused by those elements that we "flip"
				var change = 0;
				// direction of traversal
				var direction = q > 0 ? -1 : +1;
				// offset from index
				var step = 0;
				// the location where our "flips" ended
				var completedAt = -1;
				while (true)
				{
					// compute new location
					var location = index + direction * step;
					// exit if out of bounds
					if (location >= 0 && location < arr.Length)
					{
						// exit if all subsequent elements will not be flipped
						if (Math.Abs(arr[location]) >= Math.Abs(q) && location != index)
						{
							break;
						}

						// compute the change
						var was = Math.Abs(arr[location]);
						var become = Math.Abs(arr[location] + q);
						change += become - was;

						completedAt = location;
						step++;
					}
					else
					{
						break;
					}
				}

				int newSum;
				if (completedAt == -1)
				{
					// if we did not flip anything, just sum left and right
					newSum = sum + index * (-q) + (arr.Length - index) * q;
				}
				else
				{
					// if we did flip, sum left, right and changes
					newSum = sum + Math.Min(index, completedAt) * (-q) + change + (arr.Length - Math.Max(index, completedAt) - 1) * q;
				}

				result.Add(newSum);
				added += query;
			}

			return result.ToArray();
		}
	}
}
