using System;
using System.Collections.Generic;

namespace CodingInterview.HackerRank
{
	/// <summary>
	/// A binary maze is a maze made out of 0s and 1s.
	/// Each 1 represents a path through the maze while each 0 represent a blockade.
	/// The starting point is represented as a Y and the finish point is represented as X.
	/// The objective of this challenge is to find a solution to the maze by stating the direction of each from X to Y by moving through only 1s.
	/// If there are many solutions to the maze, find the shortest one.
	///
	/// https://www.hackerrank.com/contests/computing-society-code-sprint-1/challenges/binary-maze/problem
	/// </summary>
	public class BinaryMaze
	{
		/// <summary>
		/// Generates a solution to the maze represented as a series of movements
		/// </summary>
		/// <param name="start">coordinates (i, j) of the starting point (marked by Y)</param>
		/// <param name="map">the maz represented as matrix of characters(0, 1, X, Y)</param>
		/// <returns>a series of movements describing the shortest path from Y to X</returns>
		public List<char> Solve((int i, int j) start, char[,] map)
		{
			// BFS queue
			var queue = new Queue<((int i, int j) tile, List<char> path)>();
			// start with a starting tile
			queue.Enqueue((tile: start, path: new List<char>()));

			// done when all reachable tiles are visited, or earlier
			while (queue.Count > 0)
			{
				// get next tile
				var (location, path) = queue.Dequeue();

				// if it is the target tile, we are done
				if (map[location.i, location.j] == 'X')
				{
					return path;
				}

				// mark visited
				map[location.i, location.j] = '2';

				// try all possible directions (4 in this case)
				foreach (var direction in new (int i, int j, char where)[] {
					(0, +1, 'r'),
					(0, -1, 'l'),
					(+1, 0, 'd'),
					(-1, 0, 'u')
				})
				{
					// compute next tile
					var newLocation = (i: location.i + direction.i, j: location.j + direction.j);

					// do not add to queue if our of maze
					if (newLocation.i < 0 || newLocation.i == map.GetLength(0) ||
						newLocation.j < 0 || newLocation.j == map.GetLength(1)
					)
					{
						continue;
					}

					// do not add to queue if blockade or visited
					if (map[newLocation.i, newLocation.j] == '0' ||
						map[newLocation.i, newLocation.j] == '2')
					{
						continue;
					}

					// otherwise add the next tile and update its path
					var @copy = new List<char>(path);
					@copy.Add(direction.where);
					queue.Enqueue((
						tile: newLocation,
						path: @copy
					));
				}
			}

			// if target cannot be reached, return empty path
			return new List<char>();
		}

		// HackerRank required me to write primitive parsing
		void Entrypoint(String[] args)
		{
			var cases = Convert.ToInt32(Console.ReadLine());

			for (var @case = 0; @case < cases; @case++)
			{
				var dimensions = Console.ReadLine().Split(' ');
				var rows = Convert.ToInt32(dimensions[0]);
				var columns = Convert.ToInt32(dimensions[1]);

				var map = new char[rows, columns];
				(int i, int j) start = (0, 0);

				for (var row = 0; row < rows; row++)
				{
					var rowStrings = Console.ReadLine();
					for (var column = 0; column < columns; column++)
					{
						map[row, column] = rowStrings[column];
						if (map[row, column] == 'Y')
						{
							start = (i: row, j: column);
						}
					}
				}

				var path = Solve(start, map);

				foreach (var step in path)
				{
					Console.Write($"{step} ");
				}
				Console.WriteLine();
			}
		}
	}
}
