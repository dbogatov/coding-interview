namespace CodingInterview.HackerRank
{
	/// <summary>
	/// Given a boolean matrix, find the number of islands.
	///
	/// What is an island?
	///
	/// A group of connected 1s forms an island.
	/// For example, the below matrix contains 5 islands:
	///
	/// {1, 1, 0, 0, 0},
	/// {0, 1, 0, 0, 1},
	/// {1, 0, 0, 1, 1},
	/// {0, 0, 0, 0, 0},
	/// {1, 0, 1, 0, 1}
	///
	/// https://www.hackerrank.com/contests/crescent-practice-3rd-years/challenges/islands-1
	/// </summary>
	public class CountTheIslands
	{
		static int[,] map;

		/// <summary>
		/// Returns the number of islands
		/// </summary>
		/// <param name="map">the map matrix</param>
		/// <returns>the number of islands</returns>
		public int Solve(int[,] map)
		{
			// more lightweight than passing the array (or its pointer) around
			CountTheIslands.map = map;

			var islands = 0;
			for (var i = 0; i < map.GetLength(0); i++)
			{
				for (var j = 0; j < map.GetLength(1); j++)
				{
					if (DFS(i, j))
					{
						islands++;
					}
				}
			}

			CountTheIslands.map = null;

			return islands;
		}

		/// <summary>
		/// Traverses in a DFS manner.
		/// Marks visited cells.
		/// Returns true if a search found an island.
		/// </summary>
		static bool DFS(int i, int j)
		{
			if (
				i < 0 || i == map.GetLength(0) || // i out of bounds
				j < 0 || j == map.GetLength(1) || // j out of bounds
				map[i, j] == 0 || // sea
				map[i, j] == 2) // been there
			{
				return false;
			}

			// mark the visit
			map[i, j] = 2;

			// I could have manually written down 8 directions;
			// but this way I can go 3D and beyond :)
			foreach (var vertical in new int[] { -1, 0, 1 })
			{
				foreach (var horizontal in new int[] { -1, 0, 1 })
				{
					// save a call by not checking oneself
					if (!(vertical == 0 && horizontal == 0))
					{
						DFS(i + vertical, j + horizontal);
					}
				}
			}

			return true;
		}
	}
}
