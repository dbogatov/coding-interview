namespace CodingInterview.HackerRank
{
	/// <summary>
	/// Your goal is to find the number of ways to construct an array such that consecutive positions contain different values.
	///
	/// Specifically, we want to construct an array with n elements such that each element between 1 and k, inclusive.
	/// We also want the first and last elements of the array to be 1 and x.
	///
	/// Given n, k and x, find the number of ways to construct such an array.
	/// Since the answer may be large, only find it modulo 10^9+7.
	///
	/// https://www.hackerrank.com/challenges/construct-the-array/problem
	/// </summary>
	public class ConstructArray
	{
		/// <summary>
		/// The number of ways to construct the array such that consecutive elements are distinct (module 10^9+7)
		/// </summary>
		/// <param name="n">Total number of elements in array [3, 10^5]</param>
		/// <param name="k">Each element is from 1 to k inclusive [2, 10^5]</param>
		/// <param name="x">The rightmost element of array (1 is the leftmost) [1, k]</param>
		/// <returns>The number of ways to construct the array such that consecutive elements are distinct</returns>
		public long Solve(int n, int k, int x)
		{
			long[] E = new long[n + 1];
			long[] N = new long[n + 1];

			const long modulo = 1000000007L;

			for (int i = 3; i <= n; i++)
			{
				if (i == 3)
				{
					E[i] = k - 1;
					N[i] = k - 2;
				}
				else
				{
					E[i] = (k - 1) * N[i - 1] % modulo;
					N[i] = ((k - 2) * N[i - 1] % modulo + E[i - 1] % modulo) % modulo;
				}
			}
			return x == 1 ? E[n] : N[n];
		}
	}
}
